// GENERATED AUTOMATICALLY FROM 'Assets/Controls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Controls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Controls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Controls"",
    ""maps"": [
        {
            ""name"": ""Game"",
            ""id"": ""bf443ee9-c246-4298-88b1-16c4405a3e3b"",
            ""actions"": [
                {
                    ""name"": ""Movement"",
                    ""type"": ""Value"",
                    ""id"": ""30178a4d-cc4a-4c9f-a600-7f507b4803c2"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Look"",
                    ""type"": ""Value"",
                    ""id"": ""dfcd1f9d-c64b-473e-ba6d-4103b6f102c3"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Grapple"",
                    ""type"": ""Button"",
                    ""id"": ""5706da31-cda8-4706-bf3e-1ad6d54b15dd"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Reset"",
                    ""type"": ""Button"",
                    ""id"": ""a4840e2d-0c58-47e2-92d8-89cbe11a3bd4"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""EditorPause"",
                    ""type"": ""Button"",
                    ""id"": ""27a80284-6550-44b6-a0aa-0b5e3e5b3e3b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""Keyboard"",
                    ""id"": ""ab12ea1a-82d0-4844-80e9-d02edb468433"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""07094fd8-7c30-49fc-9eea-e902abb632cf"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""56cd0584-f6cb-4886-984a-b52e89528511"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""269ee058-35e9-4fe1-b7b9-ab306c9257ab"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""b33a6fc8-847f-4c36-be91-2e27276951ee"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""80d879a7-a75e-4df8-a5dd-622ff50895af"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3d97fec1-a13b-4276-8fc5-011fcfd7593d"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Grapple"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3a9f3742-2138-460b-8a2d-91f4dffca5d9"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Reset"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0322b4fe-dcbf-4028-be6c-fbdfb15fe013"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""EditorPause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Intro"",
            ""id"": ""c64e3f7f-e848-49c6-a2da-7f5af17d4da2"",
            ""actions"": [
                {
                    ""name"": ""Continue"",
                    ""type"": ""Button"",
                    ""id"": ""93a7061c-a612-4b17-ae76-3d757b08dcdd"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""9e9afc7f-43ed-4488-b7d8-b77956380c6d"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Continue"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Game
        m_Game = asset.FindActionMap("Game", throwIfNotFound: true);
        m_Game_Movement = m_Game.FindAction("Movement", throwIfNotFound: true);
        m_Game_Look = m_Game.FindAction("Look", throwIfNotFound: true);
        m_Game_Grapple = m_Game.FindAction("Grapple", throwIfNotFound: true);
        m_Game_Reset = m_Game.FindAction("Reset", throwIfNotFound: true);
        m_Game_EditorPause = m_Game.FindAction("EditorPause", throwIfNotFound: true);
        // Intro
        m_Intro = asset.FindActionMap("Intro", throwIfNotFound: true);
        m_Intro_Continue = m_Intro.FindAction("Continue", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Game
    private readonly InputActionMap m_Game;
    private IGameActions m_GameActionsCallbackInterface;
    private readonly InputAction m_Game_Movement;
    private readonly InputAction m_Game_Look;
    private readonly InputAction m_Game_Grapple;
    private readonly InputAction m_Game_Reset;
    private readonly InputAction m_Game_EditorPause;
    public struct GameActions
    {
        private @Controls m_Wrapper;
        public GameActions(@Controls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Movement => m_Wrapper.m_Game_Movement;
        public InputAction @Look => m_Wrapper.m_Game_Look;
        public InputAction @Grapple => m_Wrapper.m_Game_Grapple;
        public InputAction @Reset => m_Wrapper.m_Game_Reset;
        public InputAction @EditorPause => m_Wrapper.m_Game_EditorPause;
        public InputActionMap Get() { return m_Wrapper.m_Game; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(GameActions set) { return set.Get(); }
        public void SetCallbacks(IGameActions instance)
        {
            if (m_Wrapper.m_GameActionsCallbackInterface != null)
            {
                @Movement.started -= m_Wrapper.m_GameActionsCallbackInterface.OnMovement;
                @Movement.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnMovement;
                @Movement.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnMovement;
                @Look.started -= m_Wrapper.m_GameActionsCallbackInterface.OnLook;
                @Look.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnLook;
                @Look.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnLook;
                @Grapple.started -= m_Wrapper.m_GameActionsCallbackInterface.OnGrapple;
                @Grapple.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnGrapple;
                @Grapple.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnGrapple;
                @Reset.started -= m_Wrapper.m_GameActionsCallbackInterface.OnReset;
                @Reset.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnReset;
                @Reset.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnReset;
                @EditorPause.started -= m_Wrapper.m_GameActionsCallbackInterface.OnEditorPause;
                @EditorPause.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnEditorPause;
                @EditorPause.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnEditorPause;
            }
            m_Wrapper.m_GameActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Movement.started += instance.OnMovement;
                @Movement.performed += instance.OnMovement;
                @Movement.canceled += instance.OnMovement;
                @Look.started += instance.OnLook;
                @Look.performed += instance.OnLook;
                @Look.canceled += instance.OnLook;
                @Grapple.started += instance.OnGrapple;
                @Grapple.performed += instance.OnGrapple;
                @Grapple.canceled += instance.OnGrapple;
                @Reset.started += instance.OnReset;
                @Reset.performed += instance.OnReset;
                @Reset.canceled += instance.OnReset;
                @EditorPause.started += instance.OnEditorPause;
                @EditorPause.performed += instance.OnEditorPause;
                @EditorPause.canceled += instance.OnEditorPause;
            }
        }
    }
    public GameActions @Game => new GameActions(this);

    // Intro
    private readonly InputActionMap m_Intro;
    private IIntroActions m_IntroActionsCallbackInterface;
    private readonly InputAction m_Intro_Continue;
    public struct IntroActions
    {
        private @Controls m_Wrapper;
        public IntroActions(@Controls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Continue => m_Wrapper.m_Intro_Continue;
        public InputActionMap Get() { return m_Wrapper.m_Intro; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(IntroActions set) { return set.Get(); }
        public void SetCallbacks(IIntroActions instance)
        {
            if (m_Wrapper.m_IntroActionsCallbackInterface != null)
            {
                @Continue.started -= m_Wrapper.m_IntroActionsCallbackInterface.OnContinue;
                @Continue.performed -= m_Wrapper.m_IntroActionsCallbackInterface.OnContinue;
                @Continue.canceled -= m_Wrapper.m_IntroActionsCallbackInterface.OnContinue;
            }
            m_Wrapper.m_IntroActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Continue.started += instance.OnContinue;
                @Continue.performed += instance.OnContinue;
                @Continue.canceled += instance.OnContinue;
            }
        }
    }
    public IntroActions @Intro => new IntroActions(this);
    public interface IGameActions
    {
        void OnMovement(InputAction.CallbackContext context);
        void OnLook(InputAction.CallbackContext context);
        void OnGrapple(InputAction.CallbackContext context);
        void OnReset(InputAction.CallbackContext context);
        void OnEditorPause(InputAction.CallbackContext context);
    }
    public interface IIntroActions
    {
        void OnContinue(InputAction.CallbackContext context);
    }
}
