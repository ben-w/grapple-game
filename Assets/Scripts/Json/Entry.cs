using System;
using Newtonsoft.Json;

public class Entry
{
    [JsonProperty("name")]
    public string Name { get; private set; }
    [JsonProperty("score")]
    private string _score { get; set; }

    public TimeSpan Time
    {
        get
        {
            if (_time == TimeSpan.Zero)
            {
                _time = TimeSpan.FromTicks(long.Parse(_score));
            }
            return _time;
        }
    }

    private TimeSpan _time = TimeSpan.Zero;
}
