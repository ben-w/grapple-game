using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class Highscores : MonoBehaviour
{
    private const string BASE_URL = "http://dreamlo.com/lb/";
    
    private const string PRIVATE_CODE = "";
    private const string PUBLIC_CODE = "";

    [SerializeField]
    private GameObject _prefab;

    [SerializeField]
    private Transform _content;

    [SerializeField]
    private PanelSlider _rightPanel;

    public void GetHighscores()
    {
        if (string.IsNullOrEmpty(PUBLIC_CODE))
            return;
        StartCoroutine(GetRoutine());
    }

    private IEnumerator GetRoutine()
    {
        using (UnityWebRequest request = UnityWebRequest.Get($"{BASE_URL}{PUBLIC_CODE}/json"))
        {
            yield return request.SendWebRequest();
            
            switch (request.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.ProtocolError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.LogError($"Error with web request. {request.error}");
                    break;
                case UnityWebRequest.Result.Success:
                    ProcessHighscores(request.downloadHandler.text);
                    break;
            }
        }
    }

    public void SubmitHighscore(TimeSpan time, string userName)
    {
        if (string.IsNullOrEmpty(PRIVATE_CODE))
            return;
        
        StartCoroutine(SubmitRountine(time, userName));
    }

    private IEnumerator SubmitRountine(TimeSpan time, string userName)
    {
        using (UnityWebRequest request =
            UnityWebRequest.Get($"{BASE_URL}{PRIVATE_CODE}/add-json/{userName}/{time.Ticks}"))
        {
            yield return request.SendWebRequest();
            
            switch (request.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.ProtocolError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.LogError($"Error with web request. {request.error}");
                    break;
                case UnityWebRequest.Result.Success:
                    ProcessHighscores(request.downloadHandler.text);
                    break;
            }
            
        }
    }

    private void ProcessHighscores(string json)
    {
        JObject root = JObject.Parse(json);
        JToken dreamlo = root["dreamlo"];
        JToken leaderboard = dreamlo["leaderboard"];
        JArray entries = (JArray)leaderboard["entry"];

        List<Entry> results = JsonConvert.DeserializeObject<List<Entry>>(entries.ToString());
        
        // The leaderboard is stored in ticks. Higher the tick the longer it is
        // This means from Dreamlo we need to reverse the leaderboard
        results.Reverse();
        
        DisplayHighscores(results);
    }

    private void DisplayHighscores(List<Entry> entries)
    {
        foreach (Entry entry in entries)
        {
            GameObject lastObject = Instantiate(_prefab, _content);
            Highscore highscore = lastObject.GetComponent<Highscore>();
            highscore.SetText(entry);
        }
        _rightPanel.Play();
    }
}
