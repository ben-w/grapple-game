using System.Collections;
using TMPro;
using UnityEngine;

public class DotAnimation : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _textMeshPro;

    [SerializeField, TextArea]
    private string _text;

    private WaitForSeconds _delay = new WaitForSeconds(1);
    private int count = 0;

    private void Start() => StartCoroutine(Animation());

    private IEnumerator Animation()
    {
        while (true)
        {
            if (count == 3)
            {
                _textMeshPro.text = _text;
                count = 0;
            }
            
            _textMeshPro.text += '.';
            count++;
            yield return _delay;
        }
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        if (_textMeshPro == null)
            return;
        _textMeshPro.text = _text;
    }
    #endif
}
