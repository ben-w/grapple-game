using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class PanelSlider : MonoBehaviour
{
    public UnityEvent OnComplete;
    
    [Header("Settings")]
    [SerializeField]
    private bool _playOnStart;

    [SerializeField]
    private Vector2 _finishPoint;

    [SerializeField]
    private float _duration;

    [Header("Components")]
    [SerializeField]
    private RectTransform _rect;

    private void Start()
    {
        if (!_playOnStart)
            return;
        
        Play();
    }

    public void Play()
    {
        var tween = _rect.DOAnchorPos(_finishPoint, _duration);
        tween.onComplete += () => OnComplete?.Invoke();
        tween.SetUpdate(true);
    }
}
