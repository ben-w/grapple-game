using TMPro;
using UnityEngine;

public class Highscore : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _nameText;

    [SerializeField]
    private TextMeshProUGUI _scoreText;

    public void SetText(Entry entry)
    {
        _nameText.text = entry.Name;
        _scoreText.text = $"{entry.Time.Minutes}:{entry.Time.Seconds}:{entry.Time.Milliseconds}";
    }
}