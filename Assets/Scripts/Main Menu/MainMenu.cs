using UnityEngine;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private GrappleController _grapple;
    public void LoadGame()
    {
        _grapple.Release();
        SceneLoader.LoadScene(SceneLoader.Scenes.Game);
    }
    
}
