using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class NameInput : MonoBehaviour
{
    private const string USERNAME_KEY = "Username";
    public static string UserName;
    
    [SerializeField]
    private TMP_InputField _inputField;

    [SerializeField]
    private GameObject _invalidWarning;

    [SerializeField]
    private Button _playButton;

    private void Start()
    {
        if (PlayerPrefs.HasKey(USERNAME_KEY))
        {
            _inputField.text = PlayerPrefs.GetString(USERNAME_KEY);
        }
        else
        {
            _inputField.text = $"User{Random.Range(0, 1000)}";
        }
        
        ValidateName(_inputField.text);
    }

    public void ValueChanged(string newText) => ValidateName(newText);

    private void ValidateName(string name)
    {
        bool valid = Regex.IsMatch(name, @"^[a-zA-Z0-9]+$");
        _playButton.interactable = valid;
        _invalidWarning.SetActive(!valid);
        
        if (!valid)
            return;

        UserName = name;
        PlayerPrefs.SetString(USERNAME_KEY, UserName);
        PlayerPrefs.Save();
    }
}
