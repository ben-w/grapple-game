using System;
using System.Diagnostics;
using TMPro;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public bool IsRunning => _stopwatch.IsRunning;
    
    [SerializeField]
    private TextMeshProUGUI _displayText;

    private Stopwatch _stopwatch = new Stopwatch();

    public void StartTimer()
    {
        _stopwatch.Restart();
    }

    public TimeSpan StopTimer()
    {
        _stopwatch.Stop();
        return _stopwatch.Elapsed;
    }

    private void Update()
    {
        if (!IsRunning)
            return;
        
        TimeSpan timeSpan = _stopwatch.Elapsed;
        _displayText.text = $"{timeSpan.Minutes}:{timeSpan.Seconds}:{timeSpan.Milliseconds}";
    }
}
