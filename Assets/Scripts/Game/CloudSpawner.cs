using System.Collections.Generic;
using UnityEngine;

public class CloudSpawner : MonoBehaviour
{
    #if UNITY_EDITOR
    [SerializeField]
    private List<GameObject> _cloudPrefabs;

    [SerializeField]
    private int _amount = 100;

    [SerializeField]
    private Vector3 _scale = Vector3.one;

    [SerializeField]
    private BoxCollider _collider;

    [ContextMenu("Spawn Clouds")]
    private void SpawnClouds()
    {
        int count = 0;
        GameObject[] allChildren = new GameObject[transform.childCount];
        foreach (Transform child in transform)
        {
            allChildren[count] = child.gameObject;
            count++;
        }

        foreach (GameObject child in allChildren)
        {
            DestroyImmediate(child.gameObject);
        }
        
        Bounds bounds = _collider.bounds;
        for (int i = 0; i < _amount; i++)
        {
            GameObject randomPrefab = _cloudPrefabs[Random.Range(0, _cloudPrefabs.Count - 1)];
            GameObject lastCloud = Instantiate(randomPrefab, transform);
            lastCloud.transform.position = RandomPoint(ref bounds);
            lastCloud.transform.localScale = _scale;
        }
    }

    private Vector3 RandomPoint(ref Bounds bounds)
    {
        return new Vector3(
            Random.Range(bounds.min.x, bounds.max.x),
            Random.Range(bounds.min.y, bounds.max.y),
            Random.Range(bounds.min.z, bounds.max.z));
    }

#endif
}
