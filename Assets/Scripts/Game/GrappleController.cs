using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class GrappleController : MonoBehaviour
{
    public Controls Input;

    [SerializeField]
    private float _jointSpring = 1000;
    
    [SerializeField]
    private Rigidbody _rigidbody;

    public Transform Camera;

    [SerializeField]
    private SpringJoint _joint;

    [SerializeField]
    private LineRenderer _line;

    [SerializeField]
    private bool _grappleOnStart = false;

    [SerializeField]
    private GameObject _objectToGrapple;

    [SerializeField]
    private bool _disableControls;

    [SerializeField]
    private PlayerController _player;
    
    private void Start()
    {
        Input = new Controls();
        if (!_disableControls)
        {
            Input.Enable();
        }

        if (_grappleOnStart)
        {
            _grappleOnStart = false;
            Vector3 pos = _objectToGrapple.transform.position;
            Grapple(pos, _objectToGrapple, Vector3.Distance(pos, _rigidbody.position));
        }
    }

    public void EnableGrapple()
    {
        Input.Game.Grapple.performed += OnGrappled;
        Input.Game.Grapple.canceled += OnReleased;
        Input.Game.Reset.performed += Reset;
        Input.Game.EditorPause.performed += EditorPressed;
    }

    public void DisableGrapple()
    {
        Input.Game.Grapple.performed -= OnGrappled;
        Input.Game.Grapple.canceled -= OnReleased;
        Input.Game.Reset.performed -= Reset;
        Input.Game.EditorPause.performed -= EditorPressed;
    }

    private void Reset(InputAction.CallbackContext obj) => _player.Reset();

    public void ToggleControls(bool isEnabled)
    {
        if (isEnabled)
        {
            Input.Enable();
            return;
        }
        
        Input.Disable();
    }

    private void OnReleased(InputAction.CallbackContext obj) => Release();

    private void OnGrappled(InputAction.CallbackContext context)
    {
        if (!Physics.Raycast(
            Camera.transform.position,
            Camera.forward,
            out RaycastHit hitInfo)) 
            return;

        Grapple(hitInfo.point, hitInfo.collider.gameObject, hitInfo.distance);
    }

    private void Grapple(Vector3 hitPoint, GameObject hitObject, float distance)
    {
        Rigidbody targetRB = hitObject.GetComponent<Rigidbody>();

        if (targetRB == null)
            return;
            
        if (_joint)
            Destroy(_joint);
        
        _joint = _rigidbody.gameObject.AddComponent<SpringJoint>();
        _joint.autoConfigureConnectedAnchor = false;
        _joint.anchor = Vector3.zero;
        _joint.spring = _jointSpring;
        _joint.connectedAnchor = hitObject.transform.InverseTransformPoint(hitPoint);
        _joint.connectedBody = targetRB;
        _joint.maxDistance = distance * 0.9f;
    }

    public void Release()
    {
        if (_joint)
        {
            Destroy(_joint);
        }
        
        _line.SetPosition(0, Vector3.zero);
        _line.SetPosition(1, Vector3.zero);
    }

    private void LateUpdate()
    {
        if (!_joint) 
            return;

        _line.SetPosition(0, _rigidbody.position);
        _line.SetPosition(1, _joint.connectedBody.transform.TransformPoint(_joint.connectedAnchor));
    }

    private void EditorPressed(InputAction.CallbackContext obj)
    {
        DateTime now = DateTime.Now;
        ScreenCapture.CaptureScreenshot($"{now.Hour}-{now.Minute}-{now.Second}-{now.Millisecond}.png", 4);
    }
    
}
