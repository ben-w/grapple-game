using System;
using Cinemachine;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameManager : MonoBehaviour
{
    private const string HIGHSCORE_KEY = "Highscore";
    private enum Canvas
    {
        Intro,
        Game,
        Finish
    }

    [SerializeField]
    private float _finishSpeed = 0.1f;

    [SerializeField]
    private float _timeToSpeed = 0.5f;
    
    [Header("Components")]
    
    [SerializeField]
    private Timer _timer;

    [SerializeField]
    private GrappleController _grapple;

    [SerializeField]
    private PlayerController _player;

    [SerializeField]
    private CinemachineFreeLook _freeLook;

    [SerializeField]
    private GameObject _finishCanvas;

    [SerializeField]
    private GameObject _gameCanvas;

    [SerializeField]
    private GameObject _introCanvas;

    [SerializeField]
    private PanelSlider _leftPanel;

    [SerializeField]
    private TextMeshProUGUI _lastTimeText;
    
    [SerializeField]
    private TextMeshProUGUI _highScoreText;

    [SerializeField]
    private Highscores _highscores;

    private bool _slowMoActive;
    private float _currentTime;
    
    private void OnEnable()
    {
        _grapple = SceneLoader.PlayerObject.GetComponent<GrappleController>();
        _player = _grapple.GetComponent<PlayerController>();
        
        _grapple.Input.Game.Reset.performed += Reset;
        _player.ToggleFreeze(true);
        ShowCanvas(Canvas.Intro);
    }

    public void StartGame()
    {
        _timer.StartTimer();
        ShowCanvas(Canvas.Game);
        _player.Reset();
        _grapple.EnableGrapple();
    }
    
    private void OnDisable()
    {
        _grapple.Input.Game.Reset.performed -= Reset;
    }

    public void Finished()
    {
        // Finished gets called multiple times by each collider on the ragdoll
        if (_slowMoActive)
            return;
        
        _slowMoActive = true;
        DisableControls();
        TimeSpan lastTime = _timer.StopTimer();
        ShowCanvas(Canvas.Finish);
        _leftPanel.Play();
        CheckLastScore(lastTime, out TimeSpan highScore);
        DisplayScores(lastTime, highScore);
    }

    private void Update()
    {
        if (_slowMoActive)
        {
            SlowMo();
        }
    }

    private void SlowMo()
    {
        _currentTime += Time.deltaTime;
        Time.timeScale = Mathf.Lerp(1, _finishSpeed,  _currentTime / _timeToSpeed);
        if (_currentTime >= 1f)
        {
            _slowMoActive = false;
        }
    }

    private void DisableControls()
    {
        _grapple.ToggleControls(false);
        _freeLook.m_XAxis.m_MaxSpeed = 0;
        _freeLook.m_YAxis.m_MaxSpeed = 0;
        Cursor.lockState = CursorLockMode.None;
    }

    private void ShowCanvas(Canvas canvas)
    {
        _gameCanvas.SetActive(false);
        _finishCanvas.SetActive(false);
        _introCanvas.SetActive(false);
        
        switch (canvas)
        {
            case Canvas.Game:
                _gameCanvas.SetActive(true);
                break;
            case Canvas.Finish:
                _finishCanvas.SetActive(true);
                break;
            case Canvas.Intro:
                _introCanvas.SetActive(true);
                break;
        }
    }

    private void CheckLastScore(TimeSpan lastTime, out TimeSpan highScore)
    {
        if (PlayerPrefs.HasKey(HIGHSCORE_KEY))
        {
            string highscoreText = PlayerPrefs.GetString(HIGHSCORE_KEY);
            highScore = TimeSpan.Parse(highscoreText);
            
            if (lastTime >= highScore)
            {
                Debug.Log("No new high score");
                _highscores.GetHighscores();
                return;
            }
            
            Debug.Log($"New highscore of {lastTime}");
        }

        highScore = lastTime;
        _highscores.SubmitHighscore(lastTime, NameInput.UserName);
        PlayerPrefs.SetString(HIGHSCORE_KEY, lastTime.ToString());
        PlayerPrefs.Save();
    }

    private void DisplayScores(TimeSpan lastTime, TimeSpan highScore)
    {
        _lastTimeText.text = $"{lastTime.Minutes}:{lastTime.Seconds}:{lastTime.Milliseconds}";
        _highScoreText.text = $"{highScore.Minutes}:{highScore.Seconds}:{highScore.Milliseconds}";
    }

    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
    }

    public void Retry()
    {
        Time.timeScale = 1;
        SceneLoader.LoadScene(SceneLoader.Scenes.Game);
    }

    private void Reset(InputAction.CallbackContext obj) => Reset();

    private void Reset()
    {
        _timer.StopTimer();
        _timer.StartTimer();
    }

    public void OutOfBounds()
    {
        _grapple.Release();
        _player.Reset();
        Reset();
    }
    
}
