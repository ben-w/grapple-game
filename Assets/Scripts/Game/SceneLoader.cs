using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public static GameObject PlayerObject => _instance._playerObject;
    
    private static SceneLoader _instance;
    public enum Scenes
    {
        MainMenu,
        Loading,
        Game,
        None = -1
    }

    
    [SerializeField]
    private GameObject _playerObject;

    [SerializeField]
    private Rigidbody _playerRB;

    [SerializeField]
    private float _maxFallSpeed = 10;

    private float _maxSpeedSqr;

    private Scenes _currentScene;
    private Scenes _targetScene = Scenes.None;

    [SerializeField]
    private List<Rigidbody> _rigidbodies = new List<Rigidbody>();

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
            return;
        }

        _instance = this;
        DontDestroyOnLoad(gameObject);
        
        SceneManager.sceneLoaded += OnSceneLoaded;
        _currentScene = Scenes.MainMenu;

        _maxSpeedSqr = _maxFallSpeed * _maxFallSpeed;
    }

    public static void LoadScene(Scenes scene) => _instance.LoadSceneRoutine(scene);

    private void LoadSceneRoutine(Scenes scene)
    {
        _targetScene = scene;
        SceneManager.LoadSceneAsync((int)Scenes.Loading, LoadSceneMode.Additive);
    }
    
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.buildIndex == (int)Scenes.Loading)
        {
            SceneManager.MoveGameObjectToScene(PlayerObject, scene);
            SceneManager.UnloadSceneAsync((int)_currentScene);
            _currentScene = Scenes.Loading;
            SceneManager.LoadSceneAsync((int)_targetScene, LoadSceneMode.Additive);
            return;
        }

        if (scene.buildIndex != (int)_targetScene)
            return;
        
        SceneManager.MoveGameObjectToScene(PlayerObject, scene);
        SceneManager.UnloadSceneAsync((int)_currentScene);
        _currentScene = _targetScene;
    }

    private void FixedUpdate()
    {
        if (_currentScene != Scenes.Loading)
            return;
        
        Vector3 velocity = _playerRB.velocity;
        if (velocity.y < -_maxFallSpeed)
        {
            foreach (Rigidbody body in _rigidbodies)
            {
                Vector3 speed = body.velocity;
                speed.y = -_maxFallSpeed;
                body.velocity = speed;
            }
        }
    }

    #if UNITY_EDITOR
    [ContextMenu("Get Player Rigidbodies")]
    private void AddAllRigidbodies() => _rigidbodies = _playerObject.transform.GetComponentsInChildren<Rigidbody>().ToList();
    #endif
}
