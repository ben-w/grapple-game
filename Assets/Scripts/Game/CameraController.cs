using System.Collections;
using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class CameraController : MonoBehaviour
{

    [SerializeField]
    private float _introSpeed;
    
    [SerializeField]
    private CinemachineFreeLook _freeLook;

    [SerializeField]
    private CinemachineVirtualCamera _introCamera;

    [SerializeField]
    private CinemachineSmoothPath _introPath;

    [SerializeField]
    private GrappleController _grapple;

    [SerializeField]
    private GameManager _manager;

    [SerializeField]
    private CinemachineBrain _brain;

    private void OnEnable()
    {
        SceneManager.sceneLoaded += SceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= SceneLoaded;
    }
    
    private void SceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.buildIndex != 2)
            return;
        
        Cursor.lockState = CursorLockMode.Locked;
        Transform playerT = SceneLoader.PlayerObject.transform;
        _freeLook.m_LookAt = playerT;
        _freeLook.m_Follow = playerT;
        
        _grapple = playerT.GetComponent<GrappleController>();
        _grapple.Camera = transform;

        _grapple.Input.Intro.Continue.performed += Continue;
        StartCoroutine(PlayIntro());
        _grapple.ToggleControls(true);
        _grapple.DisableGrapple();
    }

    private IEnumerator PlayIntro()
    {
        float currentPosition = 0f;
        var trackedDolly = _introCamera.GetCinemachineComponent<CinemachineTrackedDolly>();

        while (currentPosition <= _introPath.MaxPos)
        {
            currentPosition += Time.deltaTime * _introSpeed;
            trackedDolly.m_PathPosition = currentPosition;
            yield return null;
        }
    }
    
    private void Continue(InputAction.CallbackContext context)
    {
        SwitchCamera();
        _grapple.Input.Intro.Continue.performed -= Continue;
    }

    private void SwitchCamera()
    {
        _introCamera.gameObject.SetActive(false);
        StartCoroutine(WaitForBlend());
    }
    
    private IEnumerator WaitForBlend()
    {
        while (_brain.IsBlending)
        {
            yield return null;
        }
        
        _manager.StartGame();
    }
}
