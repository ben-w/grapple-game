using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private GrappleController _grapple;
    
    [SerializeField]
    private Rigidbody _rigidbody;

    [SerializeField]
    private List<Rigidbody> _rigidbodies = new List<Rigidbody>();
    
    public void Reset()
    {
        GameObject spawn = GameObject.FindWithTag("Respawn");
        MoveRigidbodies(spawn.transform.position);
        _grapple.ToggleControls(true);
        ToggleFreeze(false);
    }

    public void MoveRigidbodies(Vector3 position)
    {
        foreach (Rigidbody body in _rigidbodies)
        {
            Vector3 offset = _rigidbody.position - body.position;
            body.position = position - offset;
            body.velocity = Vector3.zero;
        }
        
        _rigidbody.position = position;
        _rigidbody.velocity = Vector3.zero;
    }

    public void ToggleFreeze(bool isFrozen)
    {
        foreach (Rigidbody body in _rigidbodies)
        {
            body.isKinematic = isFrozen;
        }

        _rigidbody.isKinematic = isFrozen;
    }
    
    #if UNITY_EDITOR
    [ContextMenu("Add Child Rigidbodies")]
    private void AddAllRigidbodies() => _rigidbodies = GetComponentsInChildren<Rigidbody>().ToList();
#endif
}
