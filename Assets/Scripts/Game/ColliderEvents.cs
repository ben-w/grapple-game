using UnityEngine;
using UnityEngine.Events;

public class ColliderEvents : MonoBehaviour
{
    public UnityEvent<Collider> TriggerEnter;
    private void OnTriggerEnter(Collider other) => TriggerEnter?.Invoke(other);
}
