![Banner Image](https://img.itch.zone/aW1nLzcxOTczNzkucG5n/original/4RKk5v.png)
# Grapple Game
The goal of Grapple Game is to grapple to work as quickly as possible. This game was made in just under two weeks for the Into Games Industry Brief.

Find out more on the Itch.io page [here](https://marsh-mello.itch.io/grapple-game)
